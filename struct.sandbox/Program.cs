﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nest;
using Struct.PIM.Api.Client.Entity;
using @struct.sandbox.Models;
using System.Collections;
using @struct.sandbox.Services;

namespace @struct.sandbox
{
    class Program
    {        
        static void Main(string[] args)
        {            
            Console.WriteLine("1: Rebuild generic index");
            Console.WriteLine("2: Build updated generic products");
            Console.WriteLine("3: Build b2b generic drawings");
            Console.WriteLine("4: Update b2b generic drawings");

            Console.WriteLine("5: Rebuild index");
            Console.WriteLine("6: Build updated products");
            var answer = Console.ReadLine();

            switch (answer)
            {
                case "1":
                    new DataService().BuildGenericData();
                    break;
                case "2":
                    new DataService().BuildGenericData(true);
                    break;
                case "3":
                    new DataService().BuildGenericProductCollectionData();
                    break;
                case "4":
                    new DataService().BuildGenericProductCollectionData(true);
                    break;
                case "5":
                    new DataService().BuildDocumentData();
                    break;
                case "6":
                    new DataService().BuildDocumentData(true);
                    break;
            }
        }                                          

    }        
}
