﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using @struct.sandbox.Models;
using Struct.PIM.Api.Client.Entity;
using Struct.PIM.Api.Client.Entity.Attributes;

namespace @struct.sandbox.Services
{
    public static class Helpers
    {
        //Helper methods
        public static List<ProductGroupDTO> BuildHierarchy(List<int> productGroupIds, Dictionary<int, ProductGroupDTO> productGroups, int languageId)
        {
            var hierarchy = new List<ProductGroupDTO>();            
            foreach (var pId in productGroupIds)
            {                                
                if (productGroups.TryGetValue(pId, out var grp))
                {
                    hierarchy.Add(productGroups[pId]);
                    if (grp.ParentId.HasValue)
                    {
                        GetParentGroup(productGroups, grp.ParentId.Value, hierarchy);
                    }
                    
                }                
            }
            return hierarchy;
        }

        private static List<ProductGroupDTO> GetParentGroup(Dictionary<int, ProductGroupDTO> productGroups, int id, List<ProductGroupDTO> hierarchy)
        {            
            var group = productGroups[id];
            hierarchy.Add(group);
            if (group.ParentId.HasValue && group.ParentId > 0)
            {
                GetParentGroup(productGroups, group.ParentId.Value, hierarchy);
            }
            else
            {
                return null;
            }
            return hierarchy;
        }

        public static long GetDocumentId(int itemId, int languageId)
        {
            return Convert.ToInt64("1" + languageId.ToString().PadLeft(3, '0') + itemId.ToString().PadLeft(15, '0'));
        }

        public static string BuildFullTextField(List<string> fullTextContainer)
        {
            return string.Join(" ", fullTextContainer);            
        }       
    }
}
