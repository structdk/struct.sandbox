﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using @struct.sandbox.Models;
using Struct.PIM.Api.Client.Entity;
using Struct.PIM.Api.Client.Entity.Attributes;

namespace @struct.sandbox.Services
{
    public class MappingService
    {
        //Map Products to a generic object that will be added to index as document
        public IDictionary<string, object> MapGenericProduct(ProductDTO product, int languageId, List<ProductGroupDTO> groups = null, List<VariantDTO> variants = null)
        {
            var expandoProduct = new ExpandoObject();
            var genericProduct = expandoProduct as IDictionary<string, object>;
            //Setup document meta data
            genericProduct["DocumentId"] = Helpers.GetDocumentId(product.Id, languageId);
            genericProduct["Id"] = product.Id;
            genericProduct["Configuration"] = product.ConfigurationAlias;
            genericProduct["languageId"] = languageId;
            genericProduct["ProductTypeAlias"] = product.ProductTypeAlias;
            genericProduct["ExternalRef"] = product.ExternalRef;
            genericProduct["Created"] = product.Created;

            //Map productgroup hierarchies
            if (groups != null && groups.Any())
                genericProduct["ProductGroups"] = MapGenericProductGroups(groups, languageId);

            //Map values for as generic properties
            foreach (var value in product.RenderedValues)
            {
                var val = value.Value.Value.GetValue(languageId)?.Value;
                    if (string.IsNullOrEmpty(val))
                        continue;
                genericProduct[value.Key] = val;
                
            }
            //Map subvalues for as generic properties
            foreach (var value in product.RenderedSubAttributeValues)
            {
                var val = value.Value.Value.GetValue(languageId)?.Value;
                    if (string.IsNullOrEmpty(val))
                        continue;
                genericProduct[value.Key] = val;                
            }
            //Map specifications for as generic properties
            foreach (var spec in product.RenderedSpecificationValues)
            {
                var val = spec.Value.Value.GetValue(languageId)?.Value;
                if (!string.IsNullOrEmpty(val))
                {
                     genericProduct["specifications"] = new KeyValuePair<string, string>(spec.Key, val);
                }               
            }
            //Map variants for as generic properties
            if (variants != null && variants.Any())
                genericProduct["Variants"] = MapGenericVariants(variants, languageId);           

            //Add fulltext field for searching
            var fullTextContainer = new List<string>();

            if (product.RenderedValues.TryGetValue("B2CDescription", out var b2cDesc))
                fullTextContainer.Add(b2cDesc.Value.GetValue(languageId)?.Value);
            
            if (product.RenderedValues.TryGetValue("B2BDescription", out var b2bDesc))
                fullTextContainer.Add(b2bDesc.Value.GetValue(languageId)?.Value);

            genericProduct["full_text"] = Helpers.BuildFullTextField(fullTextContainer);

            return genericProduct;
        }

        public List<object> MapGenericProductGroups(List<ProductGroupDTO> groups, int languageId)
        {
            var genericGroups = new List<object>();
            foreach (var group in groups)
            {
                var expandoGroup = new ExpandoObject();
                var genericGroup = expandoGroup as IDictionary<string, object>;
                genericGroup["GroupId"] = group.Id;
                genericGroup["ExternalRef"] = group.ExternalRef;
                genericGroup["Level"] = group.Level;               
                genericGroup["SortOrder"] = group.SortOrder;

                foreach (var value in group.RenderedValues)
                {
                    var val = value.Value.Value.GetValue(languageId)?.Value;
                        if (string.IsNullOrEmpty(val))
                            continue;
                    genericGroup[value.Key] = val;
                
                }
                genericGroups.Add(genericGroup);
            }
            return genericGroups;
        }
        private List<object> MapGenericVariants(List<VariantDTO> variants, int languageId)
        {            
            var genericVariants = new List<object>();
            foreach (var variant in variants)
            {
                var expandoVariant = new ExpandoObject();
                var genericVariant = expandoVariant as IDictionary<string, object>;
                genericVariant["VariantId"] = variant.Id;
                genericVariant["Name"] = variant.Name.GetValue(languageId);
                genericVariant["ExternalRef"] = variant.ExternalRef;
                genericVariant["Created"] = variant.Created;

                foreach (var value in variant.RenderedValues)
                {
                    var val = value.Value.Value.GetValue(languageId)?.Value;
                        if (string.IsNullOrEmpty(val))
                            continue;
                    genericVariant[value.Key] = val;
                
                }

                foreach (var value in variant.RenderedSubAttributeValues)
                {
                    var val = value.Value.Value.GetValue(languageId)?.Value;
                        if (string.IsNullOrEmpty(val))
                            continue;
                    genericVariant[value.Key] = val;                
                }

                foreach (var spec in variant.RenderedSpecificationValues)
                {
                    var val = spec.Value.Value.GetValue(languageId)?.Value;
                    if (!string.IsNullOrEmpty(val))
                    {
                         genericVariant["specifications"] = new KeyValuePair<string, string>(spec.Key, val);
                    }               
                }
                genericVariants.Add(genericVariant);
            }           
            return genericVariants;
        }

        //public IDictionary<string, object> MapGenericGlobalListValue(AttributeValueDTO value, int languageId)
        //{
        //    var expandoObject = new ExpandoObject();
        //    var genericList = expandoObject as IDictionary<string, object>;

        //    //Setup document meta data
        //    //genericList["DocumentId"] = Helpers.GetDocumentId(ProductDTO product.Id, languageId);

        //    genericList["languageId"] = languageId;           
        //    genericList["DrawingFile"] = value.GetSubValue("DrawingFile")?.MediaIds;           
        //    genericList["ModelYearFrom"] = value.GetSubValue("ModelYearFrom")?.X;           
        //    genericList["ModelYearTo"] = value.GetSubValue("ModelYearTo")?.X;           
        //    genericList["Brand"] = (value.GetSubValue("Brand")?.ReferencedAttributeValues?.FirstOrDefault() as AttributeValueDTO).GetSubValue("Name")?.Text;
        //    genericList["Model"] = value.GetSubValue("Model")?.Text;  
        //    genericList["Colors"] = Helpers.GetDrawingColors(value.GetSubValue("Colors")?.ReferencedAttributeValues, languageId);  
        //    genericList["Parts"] = Helpers.GetDrawingParts(value.GetSubValue("Parts").SubAttributeValues, languageId);  
            

        //    return genericList;
        //}

        #region Map products in non generic
        //Map productDTO from API to a document that can be used for indexing
        public ProductDocument MapProductDocument(ProductDTO product, List<VariantDTO> variants, int languageId, List<ProductGroupDTO> hierarchy)
        {
            var document = new ProductDocument();
            document.Id = product.Id;
            document.DocumentId = Helpers.GetDocumentId(product.Id, languageId); 
            document.LanguageId = languageId;

            document.Hierarchy = hierarchy;         
            if (variants != null)
                document.Variants = MapVariants(variants, languageId);

            document.ProductGroupIds = product.ProductGroupIds;
            document.PrimaryGroupId = product.PrimaryGroupId;                    
            //Map values done with reflection. Could be done by simply assigning values
            foreach (var value in product.RenderedValues)
            {
                var property = document.GetType().GetProperty(value.Key);
                if (property != null)
                {
                    var val = value.Value.Value.GetValue(languageId)?.Value;
                    if (string.IsNullOrEmpty(val))
                        continue;
                    if(property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(List<>)) {
                         property.SetValue(document, val.Split('/').ToList());
                        continue;
                    }

                    var covertedVal = Convert.ChangeType(val as dynamic, property.PropertyType);
                    property.SetValue(document, covertedVal);
                }
            }
            document.ItemNo = product.Values["ItemNo"]?.Text;

            foreach (var spec in product.RenderedSpecificationValues)
            {
                var val = spec.Value.Value.GetValue(languageId)?.Value;
                if (!string.IsNullOrEmpty(val))
                {
                     document.Specifications.Add(new KeyValuePair<string, string>(spec.Key, val));
                }               
            }

            var fullTextContainer = new List<string>();

            if (product.RenderedValues.TryGetValue("B2CDescription", out var b2cDesc))
                fullTextContainer.Add(b2cDesc.Value.GetValue(languageId)?.Value);
            
            if (product.RenderedValues.TryGetValue("B2BDescription", out var b2bDesc))
                fullTextContainer.Add(b2bDesc.Value.GetValue(languageId)?.Value);

            document.FullText = Helpers.BuildFullTextField(fullTextContainer);


            return document;
        }       

        //Map list of variants to variantItems
        public List<VariantItem> MapVariants(List<VariantDTO> variants, int languageId)
        {
            var variantItems = new List<VariantItem>();
            foreach (var variant in variants)
            {
                VariantItem v = new VariantItem();
                
                v.VariantId = variant.Id;
                foreach (var value in variant.RenderedValues)
                {
                    var property = v.GetType().GetProperty(value.Key);
                    if (property != null)
                    {
                        var val = value.Value.Value.GetValue(languageId)?.Value;
                        if (string.IsNullOrEmpty(val))
                            continue;
                        if(property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(List<>)) {
                             property.SetValue(v, val.Split('/').ToList());
                            continue;
                        }

                        var covertedVal = Convert.ChangeType(val as dynamic, property.PropertyType);
                        property.SetValue(v, covertedVal);
                    }
                }

                foreach (var spec in variant.RenderedSpecificationValues)
                {
                    var val = spec.Value.Value.GetValue(languageId)?.Value;
                    if (!string.IsNullOrEmpty(val))
                    {
                         v.Specifications.Add(new KeyValuePair<string, string>(spec.Key, val));
                    }               
                }

                variantItems.Add(v);
            }
            return variantItems;
        }
        #endregion
    }
}
