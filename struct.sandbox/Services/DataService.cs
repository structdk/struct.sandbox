﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Struct.PIM.Api.Client;
using System.Configuration;
using Nest;
using @struct.sandbox.Models;
using System.Dynamic;
using Struct.PIM.Api.Client.Entity;

namespace @struct.sandbox.Services
{
    public class DataService
    {
        private PIMApiFacade _pimFacade;

        private PIMApiFacade pimFacade { get
            {
                if(_pimFacade == null)
                {
                    // Get API info from configuration file.
                    var apiUrl = ConfigurationManager.AppSettings["Struct.PIM.Api.Url"];
                    var apiKey = ConfigurationManager.AppSettings["Struct.PIM.Api.Key"];
                    _pimFacade = new PIMApiFacade(apiUrl, apiKey);
                }
                return _pimFacade;
            }
        }

        #region generic document data
        public void BuildGenericData(bool runUpdate = false, List<int> productIds = null)
        {
            var languages = pimFacade.GetLanguages();           
            
            if (runUpdate)
            {
                productIds = pimFacade.GetUpdatedProductIds(DateTime.Now.AddHours(-1)).ProductIds;
                if (productIds == null || !productIds.Any())
                {
                    Console.WriteLine("No updated products found");
                    return;
                }
            }
            if (productIds == null)
            {
                productIds = pimFacade.GetProductIds();
            }            
           
            var products = pimFacade.GetProducts(productIds, includeRenderedSpecificationValues:true, includeRenderedValues:true).Products;
            var groupIds = new List<int>();
            foreach (var group in products.Select(x=>x.ProductGroupIds))
            {
                if (group != null)
                {
                    groupIds.AddRange(group);
                }                                             
            }
            groupIds = pimFacade.GetProductGroupIds();
            var productGroups = pimFacade.GetProductGroupsByIds(groupIds, includeRenderedValues:true).ProductGroups.ToDictionary(x=>x.Id);
            var variantIds = pimFacade.GetVariantIdsByProductIds(productIds);
            var variants = pimFacade.GetVariants(variantIds.Select(x=>x.Value).SelectMany(x=>x).ToList(), includeRenderedSpecificationValues:true, includeRenderedValues:true).Variants.GroupBy(x=>x.ProductId).ToDictionary(x=>x.Key, x => x.ToList());
            
            var bulkOperations = new List<IBulkOperation>();

            var mapService = new MappingService();
            
            foreach (var lang in languages)
            {
                var items = new List<ExpandoObject>();
                foreach (var product in products)
                {
                    var groups = new List<ProductGroupDTO>();
                    if (product.PrimaryGroupId.HasValue)
                    {
                        groups = Helpers.BuildHierarchy(product.ProductGroupIds, productGroups, lang.Id);
                    }

                    if (groups.Any() && groups.Any(x=>x.Alias == "Drawings"))
                    {
                        continue;
                    }
                    var v = variants[product.Id]; 
                    var genericProduct = mapService.MapGenericProduct(product, lang.Id, groups, v);
                    items.Add(genericProduct as ExpandoObject);
                }
                bulkOperations.AddRange(items.Select(x => new BulkIndexOperation<object>(x) { Id = ((dynamic)x).DocumentId, Type = "productdocument" }).Cast<IBulkOperation>().ToList());
            }

            //Setup NEST bulkoperations            
            new ElasticService().UpdateIndex(bulkOperations, runUpdate, "structgenericsandbox");
        }
       

        public void BuildGenericProductCollectionData(bool runUpdate = false, List<int> drawingIds = null)
        {
            var languages = pimFacade.GetLanguages();            

            var bulkOperations = new List<IBulkOperation>();

            var mapService = new MappingService();
            var searchFilter = new Filter();
            searchFilter.AddFilterPair("PIM.ProductGroupId", "46468");


            // if update of drawings (get changed ones)
            if (runUpdate)
            {
                drawingIds = pimFacade.GetUpdatedProductIds(DateTime.Now.AddHours(-1), searchFilter).ProductIds;
                if (drawingIds == null || !drawingIds.Any())
                {
                    Console.WriteLine("No updated products found");
                    return;
                }
            }
            if (drawingIds == null)
            {
                drawingIds = pimFacade.GetProductIds(searchFilter);   
            }            
           
            
            // setup further data and batching for indexing
            var taken = 0;
            var count = 1;
            var batchSize = 250;
            drawingIds = drawingIds.Distinct().ToList();

            var totalNumberOfProducts = drawingIds.Count();

            while (taken < drawingIds.Count())
            {
                //take next batch
                var drawingBatch = drawingIds.Skip(taken).Take(batchSize).ToList();
                var drawings = pimFacade.GetProducts(drawingBatch, includeRenderedSpecificationValues:true, includeRenderedValues:true)?.Products;

                foreach (var lang in languages)
                {
                    var items = new List<ExpandoObject>();
                    foreach (var drawing in drawings)
                    {
                        var geneDrawingValue = mapService.MapGenericProduct(drawing, lang.Id);
                        items.Add(geneDrawingValue as ExpandoObject);
                    }                    
                    bulkOperations.AddRange(items.Select(x => new BulkIndexOperation<object>(x) { Id = ((dynamic)x).DocumentId, Type = "productdocument" }).Cast<IBulkOperation>().ToList());
                }
                Console.WriteLine(taken + "/" + totalNumberOfProducts + " done");
                taken += drawingBatch.Count();
                count++;
            }                        

            //Setup NEST bulkoperations
            new ElasticService().UpdateIndex(bulkOperations, runUpdate, "structgenericdrawings");

        }

        #endregion


        #region non generic document data

        //Get all data needed and create Bulk operations (Document oriented style index)
        public void BuildDocumentData(bool runUpdate = false, List<int> productIds = null)
        {            
            var languages = pimFacade.GetLanguages();
            var productGroups = pimFacade.GetProductGroupsByIds().ProductGroups.ToDictionary(x=>x.Id);
            
            if (runUpdate)
            {
                productIds = pimFacade.GetUpdatedProductIds(DateTime.Now.AddHours(-1)).ProductIds;
                if (productIds == null || !productIds.Any())
                {
                    Console.WriteLine("No updated products found");
                    return;
                }
            }
            if (productIds == null)
            {
                productIds = pimFacade.GetProductIds();
            }            
            var products = pimFacade.GetProducts(productIds, includeRenderedSpecificationValues:true, includeRenderedValues:true).Products;
            var variantIds = pimFacade.GetVariantIdsByProductIds(productIds);
            var variants = pimFacade.GetVariants(variantIds.Select(x=>x.Value).SelectMany(x=>x).ToList(), includeRenderedSpecificationValues:true, includeRenderedValues:true).Variants.GroupBy(x=>x.ProductId).ToDictionary(x=>x.Key, x => x.ToList());      
            
            //Create a document object for each product on each language
            var productDocuments = new List<ProductDocument>();
            var mapService = new MappingService();
            foreach (var lang in languages)
            {                
                foreach (var product in products)
                {
                    var productHierarchy = new List<ProductGroupDTO>();
                    if (product.PrimaryGroupId.HasValue)
                    {
                        productHierarchy = Helpers.BuildHierarchy(product.ProductGroupIds, productGroups, lang.Id);
                    }                    
                    var v = variants[product.Id];                    
                    productDocuments.Add(mapService.MapProductDocument(product, v, lang.Id, productHierarchy));
                }
            }         

            //Setup NEST bulkoperations
            var bulkOperations = productDocuments.Select(x => new BulkIndexOperation<ProductDocument>(x) { Id = x.DocumentId }).Cast<IBulkOperation>().ToList();

            new ElasticService().UpdateIndex(bulkOperations, runUpdate, "structsandbox");
        }

        #endregion
    }
}
