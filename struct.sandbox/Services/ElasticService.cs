﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using @struct.sandbox.Models;

namespace @struct.sandbox.Services
{
    public class ElasticService
    {
        public void UpdateIndex(IEnumerable<IBulkOperation> bulkOperations, bool isUpdate, string indexAlias, int batchSize = 200)
        {
            //Connect to remote Elastic
            var elasticConfig = GetElasticClient("http://88.99.102.89", indexAlias);
            var elasticClient = elasticConfig.Client;

            //Create a new Index while writing and replace it 
            string currentIndexName = "", newIndexName = "";
            var alias = elasticClient.GetAlias(a => a.Name(indexAlias));
            bool aliasSetOnCurrentIndex = false;
            
            //if no Index exist 
            if (alias.Indices == null || alias.Indices.Count() == 0)
            {                
                int randomNumber = new Random().Next(1, 10000);
                newIndexName = indexAlias + randomNumber.ToString();

                var result = elasticClient.CreateIndex(newIndexName);
                var aliasResult = elasticClient.Alias(a => a.Add(ad => ad.Index(newIndexName).Alias(indexAlias)));
                aliasSetOnCurrentIndex = false;
            }
            else if (!isUpdate)
            {
                currentIndexName = alias.Indices.Single().Key.Name;
                int randomNumber = new Random().Next(1, 10000);
                newIndexName = indexAlias + randomNumber.ToString();
                elasticClient.DeleteIndex(newIndexName);
                var result = elasticClient.CreateIndex(newIndexName);

                //Remove our alias from old index and add it to the new one
                elasticClient.Alias(a => a
                    .Remove(remove => remove
                        .Index(currentIndexName)
                        .Alias(indexAlias)
                    )
                    .Add(add => add
                        .Index(newIndexName)
                        .Alias(indexAlias)
                    )
                );
                aliasSetOnCurrentIndex = true;
            }
            else
                currentIndexName = alias.Indices.Single().Key.Name;                        

            //Batch data and add to index
            List<IBulkResponse> responses = new List<IBulkResponse>();
            var count = 1;

            var operationsBatch = bulkOperations.Take(batchSize);
            while (operationsBatch.Count() > 0)
            {

                if (count == 1 || count % 10 == 0) // only log for every 10 bulks
                    Console.WriteLine(string.Format("UpdatedIndex : bulk : from : {0} : to : {1}", (count - 1) * batchSize, (count * batchSize)));

                responses.Add(elasticClient.Bulk(new BulkRequest()
                    {
                        Operations = operationsBatch.ToList(),
                        Refresh = Elasticsearch.Net.Refresh.False
                    }));

                //take next batch
                operationsBatch = bulkOperations.Skip(count * batchSize).Take(batchSize);

                //increment batch start
                count++;
            }                        

            elasticClient.Refresh(new RefreshRequest(newIndexName));
            if (aliasSetOnCurrentIndex)
            {
                //remove current index
                elasticClient.DeleteIndex(currentIndexName);
            }                        
            Console.WriteLine("Done writing..");
        }
        public ElasticConfig GetElasticClient(string connString, string indexAlias)
        {
            var uri = new Uri(connString);
            return new ElasticConfig(uri, indexAlias);
        }
    }
}
