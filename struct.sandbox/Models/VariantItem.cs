﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace @struct.sandbox.Models
{
    public class VariantItem
    {
        public int VariantId { get; set; }             
        public string ItemNo { get; set; }             
        public string Size { get; set; }             
        public string BikseSize { get; set; }             
        public string Color { get; set; }             
        public string EAN { get; set; }             
        public string InventoryLocation { get; set; }             
        public List<string> SupplierItemNumbers { get; set; }             
        public bool Blocked { get; set; }             
        public bool BlockedB2B { get; set; }             
        public bool Discontinued { get; set; }             
        public int PrimaryImage { get; set; }
        public List<int> AdditinalImages { get; set; }
        public List<KeyValuePair<string,string>> Specifications { get; set; } = new List<KeyValuePair<string, string>>();

        [Object(Name = "full_text")]
        public object FullText { get; set; }
    }
}
