﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace @struct.sandbox.Models
{
    public class ElasticConfig
    {
        public ElasticConfig(Uri uri, string indexAlias)
        {            
            Settings = new ConnectionSettings(uri);
            Settings.DisableDirectStreaming(true);
            Settings.BasicAuthentication("Administrator", "dibIVzmsfNgPqPgYL2iQ");
            Settings.DefaultIndex(indexAlias);
            Client = new ElasticClient(Settings);
        }
        public ConnectionSettings Settings { get; }
        public ElasticClient Client { get; }
    }
}
