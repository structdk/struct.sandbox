﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using Struct.PIM.Api.Client.Entity;

namespace @struct.sandbox.Models
{   
    [ElasticsearchType(IdProperty = "DocumentId", Name = DocumentType)]
    public class ProductDocument
    {
        public const string DocumentType = "productdocument";

        [Ignore]
        public long DocumentId { get; set; }
        public int Id { get; set; }
        public List<ProductGroupDTO> Hierarchy { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }        
        public List<VariantItem> Variants { get; set; }
        public List<int> ProductGroupIds { get; set; }
        public int? PrimaryGroupId { get; set; }
        public string ItemNo { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public bool IsCatalogProduct { get; set; }
        public string ERPProductCategory { get; set; }
        public string ERPProductGroupCode { get; set; }
        public string TaxCodes { get; set; }
        public bool Blocked { get; set; }
        public bool BlockedB2B { get; set; }
        public bool Discontinued { get; set; }
        public string GrossWeight { get; set; }
        public string PackageSize { get; set; }
        public bool SoldinPackages { get; set; }
        public string SalesUnits { get; set; }
        public int MaxSalesQty { get; set; }
        public bool IsOutlet { get; set; }
        public string IsNewUntil { get; set; }
        //Description tab
        public string ERPDescription { get; set; }
        public string B2CDescription { get; set; }
        public string B2BDescription { get; set; }
        public string B2CTeaserText { get; set; }
        public string B2BTeaserText { get; set; }
        //Media tab
        public int PrimaryImage { get; set; }
        public List<int> AdditinalImages { get; set; }
        public List<string> Documents { get; set; } = new List<string>();
        public List<string> Videos { get; set; } = new List<string>();
        public List<string> Certificates { get; set; } = new List<string>();
        public List<KeyValuePair<string,string>> Specifications { get; set; } = new List<KeyValuePair<string, string>>();

        [Object(Name = "full_text")]
        public object FullText { get; set; }
    }
}
